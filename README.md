----------

reactOcat
----------

#### Incomplete ####
 - *CommitDetailPage :* 
    - Finished fetching data for a single commit but couldn't finish the UI
   to display the files changed in the commit.
 - *Error handlers for API calls :*
    - Error in API calls are not yet handled
 - Adding Tests is pending


#### TODO ####
 - Props types validation
 - UI looks very bad styling to be done so it looks elegant
 - Pending items I thought of adding :
  - [CommitDetailPage] show lines added, deleted, or file added/deleted
  - [CommitListPage] long commit messages not wrapped on
  - TBA