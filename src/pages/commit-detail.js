import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Navbar from '../components/navbar';
import Api from '../services/github-api';
import { noop } from '../services/helpers';
import Avatar from '@material-ui/core/Avatar';

const paperStyles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

class CommitDetailPage extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      commitId: null,
      commitData: null
    }
  }

  componentWillMount = async () => {
    const {
      match: {
        params: {
          commit_id: commitId
        }
      },
      location: {
        state: {
          repoHandle
        }
      }
    } = this.props;
    const fetchSingleCommitResponse = await Api.fetchSingleCommit(repoHandle, commitId);
    const { data: commitData } = fetchSingleCommitResponse;
    this.setState({
      commitData
    })
  }

  render() {
    console.log('In render for CommitDetailPage')
    console.log(this.state);
    const { logoutHandler = noop } = this.props;
    // const { commitData: {
    //   author: {
    //     avatar_url: avatarUrl
    //   },
    //   files
    // } } = this.state;
    return (
      <div>
        <Navbar logoutHandler={logoutHandler} />
        <Grid
          container
          direction="row"
          alignItems='center'
          justify='space-between'
        >
          <h2>Commit details</h2>
          <Avatar
            src="/static/images/uxceo-128.jpg"
          />
        </Grid>
      </div>
    );
  }
}

export default CommitDetailPage;