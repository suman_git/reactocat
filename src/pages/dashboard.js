import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Navbar from '../components/navbar';
import Api from '../services/github-api';
import Button from '@material-ui/core/Button';
import { noop } from '../services/helpers';
import CommitsList from '../components/commits-list';

const paperStyles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

class DashboardPage extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      repoHandle: 'facebook/react'
    }
  }

  componentWillMount = async () => {
    this.fetchCommits();
  }

  fetchCommits = async () => {
    const { repoHandle } = this.state;
    const commitsApiResponse = await Api.getCommitsForRepository(repoHandle);
    const { data: commits } = commitsApiResponse;
    this.setState({
      commits
    });
  }

  getRepoHandle = () => {
    const { repoHandle } = this.state;
    return repoHandle;
  }

  setRepoHandleValueInState = (value) => {
    this.setState({ repoHandle: value });
  }

  render() {
    const { logoutHandler = noop, history } = this.props;
    const { commits = [], repoHandle } = this.state;
    return (
      <div>
        <Navbar logoutHandler={logoutHandler} />
        <Grid
          container
          direction="row"
          alignItems='center'
          justify='space-evenly'
        >
          <Grid item xs={8}>
            <TextField
              id="standard-name"
              margin="normal"
              label='repo_owner/repo_name'
              value={repoHandle}
              onChange={(event) => { this.setRepoHandleValueInState(event.target.value) }}
              fullWidth={true}
            />
          </Grid>
          <Grid item xs={2}>
            <Button
              variant="contained"
              color="primary"
              size='medium'
              justify="center"
              onClick={() => this.fetchCommits()}
            >
              Fetch commits
          </Button>
          </Grid>
        </Grid>
        <Grid
          container
          justify='space-around'
        >
          <CommitsList commits={commits} repoHandle={repoHandle} routerHistory={history} />
        </Grid>
      </div>
    );
  }
}

export default DashboardPage;