import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Api from '../services/github-api';
import { setItemInStorage } from '../services/helpers';
import {
  Redirect,
  withRouter
} from 'react-router-dom'

class LoginPage extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      username: '',
      password: ''
    };
  }

  setUsercredsInLocalStorage = (username, password) => {
    setItemInStorage('username', username);
    setItemInStorage('password', password);
  }

  setInputFieldValuesInState = (fieldKey, value) => {
    this.setState({ [fieldKey]: value });
  }

  startLogin = async () => {
    const {
      username,
      password
    } = this.state;
    this.setUsercredsInLocalStorage(username, password);
    Api.init();
    const getUserDetailsResponse = await Api.getUserDetails();
    const {
      data: {
        login: loginid,
        avatar_url: avatarUrl
      }
    } = getUserDetailsResponse;
    setItemInStorage('loginid', loginid);
    setItemInStorage('avatarUrl', avatarUrl);
    setItemInStorage('isLoggedIn', true);
    this.props.loginHandler();
    return this.props.history.push('/');
  }

  render() {
    // console.log('In render for LoginPage');
    // console.log('Current state is');
    // console.log(this.state);
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: '100vh' }}
      >
        <Grid item xs={3}>
          <TextField
            id="standard-name"
            label="Username"
            margin="normal"
            fullWidth
            onChange={(event) => { this.setInputFieldValuesInState('username', event.target.value) }}
          />
          <TextField
            id="standard-uncontrolled"
            label="Password"
            type="password"
            margin="normal"
            fullWidth
            onChange={(event) => { this.setInputFieldValuesInState('password', event.target.value) }}
          />
          <Button
            variant="contained"
            color="primary"
            size='medium'
            justify="center"
            onClick={() => this.startLogin()}
          >
            Login
            </Button>
        </Grid>

      </Grid>
    );
  }
}

export default withRouter(LoginPage);