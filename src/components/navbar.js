import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Navbar extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      username: '',
      password: ''
    };
  }

  handleLogout() {
    this.props.logoutHandler();
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="title" color="default" className={classes.grow}>
              react'O'cat
            </Typography>
            <Button
              color="inherit"
              onClick={() => this.handleLogout()}
            >
              Logout
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }

}

export default withStyles(styles)(Navbar);