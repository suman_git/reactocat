import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    justifyConent: 'space-around'
  },
  listItem: {
    width: '100%',
    justifyConent: 'center'
  },
});

class CommitsList extends Component {
  render() {
    const { classes, commits = [], repoHandle, routerHistory } = this.props;

    return (
      <div className={classes.root}>
        <List>
          {commits.map(commitData => (
            <ListItem
              key={commitData.sha}
              dense
              button
              className={classes.listItem}
              onClick={() => {
                const { sha: commitId } = commitData;
                routerHistory.push({
                  pathname: `commits/${commitId}`,
                  state: {
                    repoHandle
                  }
                })
              }}
            >
              <Avatar alt={commitData.author.login} src={commitData.author.avatar_url} />
              <ListItemText primary={commitData.commit.message} />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(CommitsList);