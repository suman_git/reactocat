import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import logo from './logo.svg';
import LoginPage from '../src/pages/login';
import DashboardPage from '../src/pages/dashboard';
import CommitDetailPage from '../src/pages/commit-detail';
import { userIsSignedIn } from '../src/services/helpers';
import { removeItemsFromStorage } from './services/helpers';
import './App.css';

class App extends Component {

  constructor(...args) {
    super(...args);
    this.state = {
      isLoggedIn: false
    }
  }

  componentWillMount() {
    const loggedInFlag = userIsSignedIn();
    this.setState({
      isLoggedIn: loggedInFlag
    });
  }

  logout = () => {
    console.log('In logoutHandler methods');
    const itemsToRemoveFromLocalStorage = [
      'isLoggedIn',
      'username',
      'password',
      'avatarUrl',
      'loginid'
    ]
    removeItemsFromStorage(itemsToRemoveFromLocalStorage);
    this.setState({
      isLoggedIn: false
    });
  }

  login = () => {
    this.setState({
      isLoggedIn: true
    });
  }

  checkAuthAndRedirect(Component, props) {
    const { isLoggedIn } = this.state;
    if (isLoggedIn) {
      return <Component {...props} logoutHandler={this.logout} />;
    }
    return <Redirect to="/login" />
  }

  render() {
    console.log('state in App component is');
    console.log(this.state);
    return (
      <Router>
        <Switch>
          <Route exact path="/" render={(props) => this.checkAuthAndRedirect(DashboardPage, props)} />
          <Route path='/login' render={(props) => <LoginPage {...props} loginHandler={this.login} />} />
          <Route path='/commits/:commit_id' render={(props) => <CommitDetailPage {...props} loginHandler={this.login} />} />
        </Switch>
      </Router>
    );
  }
}

export default App;
