import axios from 'axios';
import { getItemFromStorage } from './helpers';

const init = () => {
  const username = getItemFromStorage('username');
  const password = getItemFromStorage('password');
  const encodedCredentials = window.btoa(`${username}:${password}`);
  axios.defaults.headers.common['Authorization'] = `Basic ${encodedCredentials}`;
  axios.defaults.headers.common['Content-Type'] = 'application/json';
  axios.defaults.headers.common['Accept'] = 'application/json';
}

const destroy = () => {
  axios.defaults.headers.common['Authorization'] = '';
}

const getUserDetails = async () => {
  return await axios.get('https://api.github.com/user');
}

const getCommitsForRepository = async (repoHandle) => {
  return await axios.get(`https://api.github.com/repos/${repoHandle}/commits`);
}

const fetchSingleCommit = async (repoHandle, commitId) => {
  return await axios.get(`https://api.github.com/repos/${repoHandle}/commits/${commitId}`);
}

export default {
  init,
  destroy,
  getUserDetails,
  getCommitsForRepository,
  fetchSingleCommit
};