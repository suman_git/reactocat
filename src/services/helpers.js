const setItemInStorage = (item, value) => localStorage.setItem(item, JSON.stringify(value));

const getItemFromStorage = (item) => JSON.parse(localStorage.getItem(item));

const removeItemsFromStorage = (items = null) => {
  if (items) {
    if (Array.isArray(items)) {
      for (let i = 0; i < items.length; i++) {
        localStorage.removeItem(items[i]);
      }
      return;
    }
    localStorage.removeItem(items);
  }
}

// const wipeAllDataFromStorage = localStorage.clear();

const userIsSignedIn = () => {
  return getItemFromStorage('isLoggedIn')
}

const noop = () => () => { }

export {
  setItemInStorage,
  getItemFromStorage,
  userIsSignedIn,
  removeItemsFromStorage,
  noop
}